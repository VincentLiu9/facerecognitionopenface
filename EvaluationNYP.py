
import os
import cv2
import openface
import numpy as np
import sys
import shutil
import time

import aligndlib


from TrackObj import TrackObj
from CompareObj import CompareObj
from PerformanceObj import PerformanceObj

class EvaluationNYP():

    def __init__(self):

        print('init')

        self.home_dir = os.path.expanduser('~')
        self.outputPath = self.home_dir + "/Desktop/train-images"

        self.align = openface.AlignDlib(self.home_dir + "/openface/models/dlib/shape_predictor_68_face_landmarks.dat")
        # self.net = openface.TorchNeuralNet(self.home_dir + "/openface/models/openface/nn4.small2.v1.t7", imgDim=96)
        self.net = openface.TorchNeuralNet(self.home_dir + "/openface/models/new/nyp_253_30.t7", imgDim=96)

        self.train_args = {}
        self.train_args['inputDir'] = self.home_dir + "/Desktop/train-images"
        self.train_args['outputDir'] = self.home_dir + "/Desktop/aligned-images"
        self.train_args['landmarks'] = "outerEyesAndNose"
        self.train_args[
            'dlibFacePredictor'] = self.home_dir + "/openface/models/dlib/shape_predictor_68_face_landmarks.dat"
        self.train_args['size'] = 96
        self.train_args['skipMulti'] = False
        self.train_args['fallbackLfw'] = False
        self.train_args['verbose'] = True
        self.train_args['training_time'] = []

        self.match_track_list = []
        self.correct_rejection_list = []
        self.false_acceptance_list = []
        self.false_rejection_list = []

        self.performanceObj = PerformanceObj()

        # Clean training image
        shutil.rmtree(self.home_dir + "/Desktop/train-images")
        openface.helper.mkdirP(self.home_dir + "/Desktop/train-images")
        os.popen("cp -r " + self.home_dir + "/Desktop/AA " + self.home_dir + "/Desktop/train-images")


    def train(self):

        print('train...')

        # Clean aligned faces
        shutil.rmtree(self.home_dir + '/Desktop/aligned-images')
        openface.helper.mkdirP(self.home_dir + '/Desktop/aligned-images')

        train_root_dir = self.home_dir + '/Documents/NYP_830_training_253'

        track_image_dict = self.getTrackImageDict(train_root_dir)

        for track_name, image_names in track_image_dict.items():

            dict = {}

            parent = os.path.split(track_name)[1]

            dict[self.home_dir + '/Desktop/train-images/' + parent] = image_names

            # Save the track
            self.save_training_imgs(dict)

        # Train
        self.trainModel()


    def trainModel(self):

        print('trainModel')

        # Step 0: Clean cache
        if os.path.isfile(self.home_dir + '/openface/training/work/trainCache.t7'):

            os.remove(self.home_dir + '/openface/training/work/trainCache.t7')

        # Step 1: Write aligned image into aligned-images
        aligndlib.alignMain(self.train_args)

        # Do the following work in command line.....

        # Step 2:
        # os.popen('cd ~/openface/training')
        # os.popen('~/openface/util/prune-dataset.py ~/Desktop/aligned-images/ --numImagesThreshold 3')
        #
        # tmp = os.popen('th ~/openface/training/main.lua -data ~/Desktop/aligned-images').readlines()readlines


    def create_output_folder(self, folder_name):

        print("Create output folder: {}".format(folder_name))

        if os.path.isdir(folder_name):

            shutil.rmtree(folder_name)

        os.mkdir(folder_name)

    def takeSnapshot(self, folder_name, crop_img, file_name):

        filename = "{}.jpg".format(file_name)
        writePath = folder_name + "/" + filename

        cv2.imwrite(writePath, crop_img)

        print("[INFO] save {}".format(writePath))

    def save_training_imgs(self, training_dict):

        for folder_name, imgs in training_dict.items():

            self.create_output_folder(folder_name)

            for img_idx, img in enumerate(imgs):

                bgr_img = cv2.imread(img)

                self.takeSnapshot(folder_name, bgr_img, str(img_idx))

    def test(self):

        print('test')

        test_root_dir = self.home_dir + '/Documents/NYP_830_testing_253'

        # Get dictionary<track_name, image_names>
        track_image_dict = self.getTrackImageDict(test_root_dir)

        # Calculate the features, get dictionary<track_name, features>
        track_features_dict = self.getTrackFeatureDict(track_image_dict)

        # Convert the dict<track, features> to list[trackObj]
        track_features_list = self.convertTrackDictToList(track_features_dict)

        # Do comparision between tracks, get list[compareObj]
        match_cnt, compare_result_list = self.compare(track_features_list)

        far_list, frr_list = self.getFarFrr(match_cnt, compare_result_list)

        eer, eer_idx = self.getEerFromFrrFar(far_list, frr_list)

        print("Test track cnt:", len(track_image_dict))
        print('Match cnt (per threshold):', match_cnt)
        print("FAR:", ['{:.4f}'.format(far) for far in far_list])
        print("FRR:", ['{:.4f}'.format(frr) for frr in frr_list])
        print("EER(value/index):{}/{}".format(eer, eer_idx))

        for i in range(6):

            print('[{}]'.format(i))

            print('Correct accept:', self.match_track_list[i])
            print('Correct reject:', self.correct_rejection_list[i])
            print('False accept:', self.false_acceptance_list[i])
            print('False reject:', self.false_rejection_list[i])

        print("Match tracks:", self.match_track_list)

        print("Average face detection time: {:.4f}".format(np.mean(np.array(self.performanceObj.getFaceDetectionTime()))))
        print("Average face alignment time: {:.4f}".format(np.mean(np.array(self.performanceObj.getFaceAlignmentTime()))))
        print("Average feature extraction time: {:.4f}".format(np.mean(np.array(self.performanceObj.getFeatureExtractionTime()))))

        print('Template comparison time:', self.performanceObj.getTrackMatchingTime())
        print('Max template comparison time: {:.4f}'.format(max(self.performanceObj.getTrackMatchingTime())))
        print('Min template comparison time: {:.4f}'.format(min(self.performanceObj.getTrackMatchingTime())))

    def convertTrackDictToList(self, dict):

        track_features_list = []

        # [Note] the order of dictionary changes every time
        for track_name, features in dict.items():

            trackObj = TrackObj()
            trackObj.setTrackName(track_name)
            trackObj.setTrackClass(os.path.split(track_name)[1].split('_')[0])
            trackObj.setFeatures(features)

            track_features_list.append(trackObj)

        return track_features_list

    def compare(self, track_features_list):

        compare_result_list = []

        match_cnt = 0

        for base_idx in range(len(track_features_list)):

            base_obj = track_features_list[base_idx]

            for compare_idx in range(base_idx + 1, len(track_features_list)):

                compare_obj = track_features_list[compare_idx]

                # Compare features of base_obj and compare_obj
                raw_scores = self.compareFeatures(base_obj.getFeatures(), compare_obj.getFeatures())

                compare_result = CompareObj()
                compare_result.setBaseClass(base_obj.getTrackClass())
                compare_result.setCompareClass(compare_obj.getTrackClass())
                compare_result.setRawScores(raw_scores)

                compare_result_list.append(compare_result)

                match_cnt += 1

        # Normalization based on the global maximum and minimum
        compare_result_list = self.normalization(compare_result_list=compare_result_list)

        return match_cnt, compare_result_list


    def getEerFromFrrFar(self, far_list, frr_list):

        diff_list = list(np.array(far_list) - np.array(frr_list))

        diff_list = list(map(abs, diff_list))

        min_idx = np.argmin(diff_list)

        return (far_list[min_idx] + frr_list[min_idx]) / 2.0, min_idx

    def isPositiveMajority(self, value_list, threshold):

        positive_cnt = np.sum(np.array(value_list) < threshold)

        return positive_cnt > (0.5 * len(value_list))

    def getFarFrr(self, match_cnt, compare_result_list):

        # thresholds = [5, 10, ..., 95]
        threshold_list = range(5, 100, 5)

        far_list = []
        frr_list = []

        for threshold in threshold_list:

            threshold = threshold/100.0

            fa_cnt = 0
            fr_cnt = 0

            match_tracks = []
            false_acceptances = []
            false_rejections = []
            correct_rejections = []

            for compare_result in compare_result_list:

                normalized_scores = compare_result.getNormalizedScores()

                is_same_class = (compare_result.getBaseClass() == compare_result.getCompareClass())

                is_positive_majority = self.isPositiveMajority(value_list=normalized_scores, threshold=threshold)

                # Evaluate as positive
                if is_positive_majority:

                    if not is_same_class:

                        # print('False accept')
                        fa_cnt += 1

                        false_acceptances.append([compare_result.getBaseClass(), compare_result.getCompareClass()])

                    else:

                        match_tracks.append(compare_result.getBaseClass())

                else:

                    if is_same_class:

                        fr_cnt += 1

                        false_rejections.append([compare_result.getBaseClass(), compare_result.getCompareClass()])

                    else:

                        correct_rejections.append([compare_result.getBaseClass(), compare_result.getCompareClass()])

            self.match_track_list.append(match_tracks)
            self.false_acceptance_list.append(false_acceptances)
            self.false_rejection_list.append(false_rejections)
            self.correct_rejection_list.append(correct_rejections)

            far_list.append(fa_cnt/match_cnt)
            frr_list.append(fr_cnt/match_cnt)

        return far_list, frr_list


    def normalization(self, compare_result_list):

        raw_score_max = 0
        raw_score_min = sys.float_info.max

        for compare_result in compare_result_list:

            raw_scores = compare_result.getRawScores()

            for raw_score in raw_scores:

                if raw_score > raw_score_max:

                    raw_score_max = raw_score

                if raw_score < raw_score_min:

                    raw_score_min = raw_score

        for compare_result in compare_result_list:

            raw_scores = compare_result.getRawScores()
            normalized_scores = []

            for raw_score in raw_scores:

                normalized_score = 1.0 * (raw_score - raw_score_min)/(raw_score_max - raw_score_min)
                normalized_scores.append(normalized_score)

            compare_result.setNormalizedScores(normalized_scores)

        return compare_result_list

    def compareFeatures(self, base_features, compare_features):

        score_list = []

        compare_time_start = time.time()

        for base_feature in base_features:

            for compare_feature in compare_features:

                if len(base_feature) != len(compare_feature):

                    continue

                dist = np.array(base_feature) - np.array(compare_feature)

                score = np.dot(dist, dist)
                score_list.append(score)

        compare_time_end = time.time()

        self.performanceObj.insertTrackMatchingTime(len(base_features) * len(compare_features), compare_time_end - compare_time_start)

        return score_list

    def getFeatureList(self, track_features_list):

        feature_list = []

        for track_name, features in track_features_list.items():

            feature_list.append(features)

        return feature_list

    def getTrackFeatureDict(self, track_image_dict):

        print('Calculating features')

        track_feature_dict = {}

        for track_name, image_names in track_image_dict.items():

            feature_list = []

            for image_name in image_names:

                bgr_img = cv2.imread(image_name)
                rgb_img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2RGB)

                # Get the feature of the image
                feature_list.append(self.getRep(rgb_img))

            track_feature_dict[track_name] = feature_list

        return track_feature_dict

    def getRep(self, rgb_img):

        detection_start_time = time.time()
        bounding_box = self.align.getLargestFaceBoundingBox(rgb_img)

        detection_end_time = time.time()

        self.performanceObj.addFaceDetectionTime(detection_end_time - detection_start_time)

        if bounding_box is None:

            return []

        else:

            align_start_time = time.time()

            aligned_face = self.align.align(96, rgb_img, bounding_box, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

            align_end_time = time.time()

            self.performanceObj.addFaceAlignmentTime(align_end_time - align_start_time)

            if aligned_face is None:

                print('Unable to align image')

                return []
            else:

                feature_extraction_start_time = time.time()

                rep = self.net.forward(aligned_face)

                feature_extraction_end_time = time.time()

                self.performanceObj.addFeatureExtractionTime(feature_extraction_end_time - feature_extraction_start_time)

                return rep


    def getTrackImageDict(self, root_dir):

        folder_list = []
        prev_folder_name = ""
        group_dict = {}

        training_imgs = []

        for dirName, subdirList, fileList in os.walk(root_dir):

            for file in fileList:

                if any(map(file.endswith, [".png", '.jpg', '.bmp'])):
                    training_imgs.append(os.path.join(dirName, file))

        for img in training_imgs:

            folder_name = os.path.dirname(img)

            if folder_name != prev_folder_name and len(
                    folder_list) != 0:  # len(folder_list) == 0 only in first iteration

                group_dict[prev_folder_name] = folder_list.copy()
                folder_list.clear()

            # Store current image path in folder_list
            folder_list.append(img)

            # Update previous folder
            prev_folder_name = folder_name

        # Store the last series of images in the same folder
        group_dict[prev_folder_name] = folder_list

        return group_dict


