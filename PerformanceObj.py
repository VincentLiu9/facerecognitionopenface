import numpy as np

class PerformanceObj:

    def __init__(self):

        self.track_matching_time = {}
        self.face_detection_time = []
        self.face_alignment_time = []
        self.feature_extraction_time = []

    def insertTrackMatchingTime(self, matching_cnt, time):

        if matching_cnt in self.track_matching_time:

            self.track_matching_time.get(matching_cnt).append(time)

        else:

            new_time_list = [time]

            self.track_matching_time[matching_cnt] = new_time_list

    def addFaceDetectionTime(self, time):

        self.face_detection_time.append(time)

    def getFaceDetectionTime(self):

        return self.face_detection_time

    def addFaceAlignmentTime(self, time):

        self.face_alignment_time.append(time)

    def getFaceAlignmentTime(self):

        return self.face_alignment_time

    def addFeatureExtractionTime(self, time):

        self.feature_extraction_time.append(time)

    def getFeatureExtractionTime(self):

        return self.feature_extraction_time

    def getTrackMatchingTime(self):

        return self.track_matching_time

    def showTrackMatchingTime(self):

        for matching_cnt, time_list in self.track_matching_time.items():

            print('[{}] {}'.format(matching_cnt, np.mean(np.array(time_list))))
