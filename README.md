### What is this repository for? ###
A testing program to evaluate the accuray of the OpenFace neural network model on NYP Cheers dataset. Below are the versions of all the libraries included in the development environment:

| Item | Version |
| ---| --- |
|Operating system|Ubuntu 16.04 LTS|
|OpenFace| 2.0|
|Torch|7|
|Dlib|19.04|
|OpenCV|3.3.0|
|Python|3.5|

### How do I get set up? ###
Follow the below order to install the enviroment. Please kindly note that there may be plenty of tricks and issues during the installation.
1. OpenCV
2. Dlib
3. Torch
3. OpenFace

Reference:
* [Ubuntu 16.04: How to install OpenCV]
* [How to install dlib]
* [How to install torch]
* [How to install Openface]

[Ubuntu 16.04: How to install OpenCV]: <https://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv/>
[How to install dlib]: <https://www.pyimagesearch.com/2017/03/27/how-to-install-dlib/>
[How to install torch]: <http://torch.ch/docs/getting-started.html#_>
[How to install openface]: <https://iqbalnaved.wordpress.com/2016/09/19/installing-openface-an-open-source-facial-behavior-analysis-toolkit/>
