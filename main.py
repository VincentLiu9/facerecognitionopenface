
import os
import psutil

from EvaluationNYP import EvaluationNYP

pid = os.getpid()

print('Pid = ', pid)
print('CPU count = ', psutil.cpu_count())

# Only use one core
p = psutil.Process()
# p.cpu_affinity([0])

print('CPU count = ', len(p.cpu_affinity()))

app = EvaluationNYP()

#app.train()

app.test()