
import unittest
from EvaluationNYP import EvaluationNYP

from CompareObj import CompareObj

class EvaluationNYPTest(unittest.TestCase):

    def setUp(self):

        self.app = EvaluationNYP()

    def tearDown(self):

        self.app = None

    def test_convertTrackDictToList(self):

        trackDict = {'/home/track1_A':[[1,2,3],[4,5,6]], '/home/track2_B':[[7, 8, 9], [10, 11, 12]]}

        track_features_list = self.app.convertTrackDictToList(dict=trackDict)

        self.assertEqual(2, len(track_features_list), 'Wrong data length')

        for track_features in track_features_list:

            if 'track1' == track_features.getTrackClass():

                self.assertEqual([[1,2,3],[4,5,6]], track_features.getFeatures())

            if 'track2' == track_features.getTrackClass():

                self.assertEqual([[7,8,9],[10,11,12]], track_features.getFeatures())


    def test_normalization(self):

        compare_result_list = []

        compareObj1 = CompareObj()
        compareObj1.setRawScores([0, 2, 4])

        compareObj2 = CompareObj()
        compareObj2.setRawScores([1, 7.5, 10])

        compare_result_list.append(compareObj1)
        compare_result_list.append(compareObj2)

        normalized_compare_result_list = self.app.normalization(compare_result_list)

        normalized_compare_result1 = normalized_compare_result_list[0]
        self.assertEqual([0.0, 0.2, 0.4], normalized_compare_result_list[0].getNormalizedScores(), 'Wrong normalized result')
        self.assertEqual([0.1, 0.75, 1.0], normalized_compare_result_list[1].getNormalizedScores(), 'Wrong normalized result')

    def test_compareFeatures(self):

        base_features = [[0.3, 0.5, 0.9],[0.4, 0.1, 0.3]]
        compare_features = [[0.7, 0.2, 0.2],[0.6, 0.1, 0.0]]

        score_list = self.app.compareFeatures(base_features, compare_features)

        self.assertAlmostEqual(0.74, score_list[0])
        self.assertAlmostEqual(1.06, score_list[1])
        self.assertAlmostEqual(0.11, score_list[2])
        self.assertAlmostEqual(0.13, score_list[3])

    def test_isPositiveMajority(self):

        value_list = [0.4, 0.6, 0.1, 0.3, 0.3, 0.9]
        threshold = 0.5

        is_majority = self.app.isPositiveMajority(value_list, threshold)

        self.assertFalse(is_majority)

        threshold = 0.2
        is_majority = self.app.isPositiveMajority(value_list, threshold)

        self.assertTrue(is_majority)
