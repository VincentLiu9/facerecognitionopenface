

class CompareObj:

    def __init__(self):

        self.base_class = ''
        self.compare_class = ''

        self.raw_scores = ''
        self.normalized_scores = ''

    def setBaseClass(self, base_class):

        self.base_class = base_class

    def getBaseClass(self):

        return self.base_class

    def setCompareClass(self, compare_class):

        self.compare_class = compare_class

    def getCompareClass(self):

        return self.compare_class

    def setRawScores(self, raw_scores):

        self.raw_scores = raw_scores

    def getRawScores(self):

        return self.raw_scores

    def setNormalizedScores(self, normalized_scores):

        self.normalized_scores = normalized_scores

    def getNormalizedScores(self):

        return self.normalized_scores