

class TrackObj:

    def __init__(self):

        self.track_name = ''
        self.track_class = ''
        self.features = []

    def setTrackName(self, track_name):

        self.track_name = track_name

    def getTrackName(self):

        return self.track_name

    def setTrackClass(self, track_class):

        self.track_class = track_class

    def getTrackClass(self):

        return self.track_class

    def setFeatures(self, features):

        self.features = features

    def getFeatures(self):

        return self.features